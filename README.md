# gym-bluerov

This repository contains a simple planar BlueROV model in the OpenAI Gym framework.

## Getting Started
Install the source using python
   ```
   python setup.py install
   ```

### Prerequisits
    - Numpy
    - Scipy
    - gym
   
## Usage

From the provided [example](examples/basic_run.py)
```python
from gymbluerov import BlueROVEnv
```
`BlueROVEnv` is the environment using based on `gym`


```python
    env = BlueROVEnv(marker=np.zeros(3),
                     dt=0.05,
                     tmax=30,
                     k1=1,
                     k2=1,
                     k3=1)
```
Starting an environment requires a marker position, a sampling rate for the integrator, a maximum simulation time, and three constants for the reward function.

```python
    env.reset()
```
Reseting the environment resets the internal integrator and resamples the state.
The function `def initialize_state(self)` is not implemented and raises an `NotIMplementederror`

```python
    @property
    def initialize_state(self):

        raise NotImplementedError()
```

This allows the user to implement a different initialize state such as in the [initialize_state](examples/initialize_state.py) example.


