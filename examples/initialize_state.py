from gymbluerov import BlueROVEnv
import numpy as np


class BlueROVEnv2(BlueROVEnv):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

    @property
    def initialize_state(self):

        return np.array([2, 2, 0, 0, 0, 0])


def main():

    env = BlueROVEnv2(marker=np.zeros(3),
                      dt=0.05,
                      tmax=30,
                      k1=1,
                      k2=1,
                      k3=1)
    env.reset()
    done = False

    while not done:

        action = env.action_space.sample()
        state, reward, done, _ = env.step(action)
    print(f'Final State: {state}, Final Reward: {reward}')


if __name__ == '__main__':

    main()
